Projet Portfolio pour le site simplonLyon



User Stories : 

- En tant qu'apprenant j'ai besoin de montrer mes compétences afin d'être reconnu par des recruteurs.
- En tant qu'apprenant j'ai besoin d'être contacté facilement pour améliorer mon réseau.
- En tant que recruteur je veux pouvoir identifier facilement les projets réalisés.
- En tant qu'apprenant je veux pouvoir me présenter rapidement pour être plus "attractif"

![](./CaptureMaquette.png)

![](./CaptureMaquetteMobile.png)